from django.shortcuts import render, get_object_or_404

from .models import Runner


def char_sheet(request, runner_id):
    runner = get_object_or_404(Runner, pk=runner_id)
    context = {
        'runner': runner
    }
    return render(request, 'charsheet/char_sheet.html', context)
