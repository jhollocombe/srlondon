from django.conf.urls import url

from . import views

app_name = 'charsheet'
urlpatterns = [
    url(r'^(?P<runner_id>[0-9]+)/$', views.char_sheet, name='char_sheet'),
]