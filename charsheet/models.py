# -*- coding: latin-1 -*-

from django.db import models
from django.contrib.auth.models import User
import random


class D6:
    def __init__(self, num=1, add=0):
        self._num = num
        self._add = add

    def __rmul__(self, num):
        return D6(num, self._add)

    def __radd__(self, add):
        return D6(self._num, self._add + add)

    def roll(self):
        return sum(random.randint(1, 6) for _ in range(self._num)) + self._add

    def __str__(self):
        return ("{0} + {1}D6".format(self._add, self._num)) if self._add else "{0}D6".format(self._num)

    def __repr__(self):
        return "D6(num={0}, add={1})".format(self._num, self._add)


class Skill(models.Model):
    name = models.CharField(max_length=256, unique=True)


class Runner(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=256)
    bod = models.IntegerField(verbose_name='Body')
    agi = models.IntegerField(verbose_name='Agility')
    rea = models.IntegerField(verbose_name='Reaction')
    str = models.IntegerField(verbose_name='Strength')
    wil = models.IntegerField(verbose_name='Will')
    log = models.IntegerField(verbose_name='Logic')
    int = models.IntegerField(verbose_name='Intuition')
    cha = models.IntegerField(verbose_name='Charisma')
    mag = models.IntegerField(verbose_name='Magic')
    res = models.IntegerField(verbose_name='Resonance')
    edge = models.IntegerField()
    nuyen = models.IntegerField(verbose_name='￥')
    essence = models.FloatField(verbose_name='Essence', default=6)
    karma = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    @property
    def phys_init(self):
        return 2 + 1 * D6()

    @property
    def astral_init(self):
        return 2 + 2 * D6()

    @property
    def coldsim_init(self):
        return 2 + 3 * D6()

    @property
    def hotsim_init(self):
        return 2 + 4 * D6()

    @property
    def phys_limit(self):
        return 2

    @property
    def mental_limit(self):
        return 2

    @property
    def social_limit(self):
        return 2


class SkillRating(models.Model):
    user = models.ForeignKey(User)
    skill = models.ForeignKey(Skill)
    rating = models.IntegerField()

    def __str__(self):
        return "{0}: {1}".format(self.skill.name, self.rating)


class Accessory(models.Model):
    name = models.CharField(max_length=256)
    mount = models.CharField(max_length=128)
    rating = models.CharField(max_length=10)
    avail = models.CharField(max_length=10)
    cost = models.IntegerField()
    source = models.CharField(max_length=10)
    page = models.IntegerField()


class ItemCategory(models.Model):
    TYPES = [
        'melee', 'exotic', 'bow', 'crossbow', 'taser', 'gun', 'cannon', 'flame', 'laser', 'glauncher', 'mlauncher'
    ]
    name = models.CharField(max_length=256, unique=True)
    type = models.CharField(max_length=128, choices=TYPES)


class Item(models.Model):
    name = models.CharField(max_length=256)
    category = models.ForeignKey(ItemCategory)
    cost = models.IntegerField()


class Gear(models.Model):
    user = models.ForeignKey(User)
    item = models.ForeignKey(Item)
    ration = models.IntegerField()
    count = models.IntegerField()


class Spell(models.Model):
    name = models.CharField(max_length=256)


class Spells(models.Model):
    user = models.ForeignKey(User)
    spell = models.ForeignKey(Spell)


class Augment(models.Model):
    name = models.CharField(max_length=256)
    

class Augments(models.Model):
    user = models.ForeignKey(User)
    augment = models.ForeignKey(Augment)
    rating = models.IntegerField()