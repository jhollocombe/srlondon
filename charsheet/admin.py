from django.contrib import admin

from .models import Skill, Runner

admin.site.register(Skill)
admin.site.register(Runner)
