from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'bbs'
urlpatterns = [
    url(r'^$', views.thread_list, name='threads'),
    url(r'^(?P<thread_id>[0-9]+)/$', views.message_list, name='messages'),
    url(r'^new/$', views.new_thread, name='new_thread'),
    url(r'^(?P<thread_id>[0-9]+)/new/$', views.new_message, name='new_message'),
    url(r'^accounts/login/$', auth_views.login, name='login'),
    url(r'^accounts/logout/$', auth_views.logout, name='logout'),
    url(r'^accounts/create/$', views.create_user, name='create_user'),
]