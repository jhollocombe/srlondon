from django.db import models
from django.contrib.auth.models import User


class Thread(models.Model):
    title = models.CharField(max_length=256)
    pub_date = models.DateTimeField(verbose_name='creation date')


class Message(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    text = models.CharField(max_length=1024)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    pub_date = models.DateTimeField(verbose_name='creation date')


class Runner(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='uploads')