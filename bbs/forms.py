from django import forms
from django.contrib.auth import forms as auth_forms
from django.utils import timezone

from .models import Thread, Message, Runner


class RunnerForm(auth_forms.UserCreationForm):
    avatar = forms.ImageField(label='Avatar', required=False)

    def save(self, commit=True):
        user = super().save(self)
        runner = Runner(user=user, avatar=self.cleaned_data['avatar'])
        if commit:
            runner.save()
        return user


class ThreadForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        model = Thread
        fields = ('title',)

    def save(self, commit=True):
        thread = forms.ModelForm.save(self, commit=False)
        thread.pub_date = timezone.now()
        thread.save()
        return thread


class MessageForm(forms.ModelForm):
    error_css_class = 'error'
    required_css_class = 'required'

    class Meta:
        model = Message
        fields = ('text',)

    def __init__(self, user, thread, *args, **kwargs):
        self.author = user
        self.thread = thread
        forms.BaseModelForm.__init__(self, *args, **kwargs)

    def save(self, commit=True):
        message = forms.ModelForm.save(self, commit=False)
        message.author = self.author
        message.thread = self.thread
        message.pub_date = timezone.now()
        message.save()
        return message
