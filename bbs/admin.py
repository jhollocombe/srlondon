from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import Thread, Message, Runner


class RunnerInline(admin.StackedInline):
    model = Runner
    can_delete = False
    verbose_name_plural = 'runner'


class UserAdmin(BaseUserAdmin):
    inlines = (RunnerInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(Thread)
admin.site.register(Message)
