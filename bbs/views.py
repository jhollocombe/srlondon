from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .models import Message, Thread
from .forms import MessageForm, ThreadForm, RunnerForm


@login_required
def thread_list(request, form=None):
    threads = Thread.objects.order_by('pub_date')
    context = {
        'threads': threads,
        'form': ThreadForm() if form is None else form,
    }
    return render(request, 'bbs/threads.html', context)


@login_required
def message_list(request, thread_id, form=None):
    thread = get_object_or_404(Thread, pk=thread_id)
    messages = Message.objects.filter(thread=thread).order_by('pub_date')
    context = {
        'thread': thread,
        'messages': messages,
        'form': MessageForm(request.user, thread) if form is None else form,
    }
    return render(request, 'bbs/messages.html', context)


@login_required
def new_thread(request):
    if request.method == 'POST':
        form = ThreadForm(request.POST)
        if form.is_valid():
            thread = form.save()
            return HttpResponseRedirect(reverse('bbs:messages', args=[thread.pk]))
        else:
            return thread_list(request, form=form)
    else:
        return thread_list(request)


@login_required
def new_message(request, thread_id):
    if request.method == 'POST':
        thread = get_object_or_404(Thread, pk=thread_id)
        form = MessageForm(request.user, thread, data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('bbs:messages', args=[thread_id]))
        else:
            return message_list(request, thread_id, form=form)
    else:
        return message_list(request, thread_id)


def create_user(request):
    context = {
        'form': RunnerForm(),
        'next': request.GET.get('next', '/'),
    }
    if request.method == 'POST':
        form = RunnerForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            user = authenticate(username=request.POST["username"], password=request.POST["password1"])
            login(request, user)
            return HttpResponseRedirect(request.POST['next'])
        else:
            context['form'] = form
            return render(request, 'registration/create_user.html', context)
    else:
        return render(request, 'registration/create_user.html', context)
