from xml.etree import ElementTree as etree

from charsheet.models import ItemCategory, Item, Accessory


def load_category(category):
    type = category.get('type')
    name = category.text
    return ItemCategory(name=name, type=type)


def find_category(name):
    return ItemCategory.get(name=name)


def load_accessory(accessory):
    fields = (
        ('name', str),
        ('mount', str),
        ('rating', str),
        ('avail', str),
        ('cost', int),
        ('source', str),
        ('page', int),
    )
    values = {}
    for (name, cls) in fields:
        values[name] = cls(accessory.find(name).text)
    return Accessory(**values)


def find_accessory(name):
    return Accessory.get(name=name)


def load_weapon(weapon):
    fields = (
        ('name', str),
        ('category', str),
        ('type', str),
        ('conceal', int),
        ('accuracy', int),
        ('reach', int),
        ('damage', str),
        ('ap', int),
        ('mode', str),
        ('rc', int),
        ('ammo', str),
        ('avail', str),
        ('cost', int),
        ('allowaccessory', bool),
        ('source', str),
        ('page', int),
    )
    values = {}
    for (name, cls) in fields:
        values[name] = cls(weapon.find(name).text)
    for mount in weapon.find('accessorymounts').iterfind('mount'):
        values['accessorymounts'] = (values.get('accessorymounts', []) + [mount.text])
    for accessory in weapon.find('accessories').iterfind('accessory'):
        name = accessory.find('name').text
        values['accessories'] = (values.get('accessories', []) + [find_accessory(name)])
    return Item(**values)


def load():
    xml = etree.parse('data/weapons.xml')
    root = xml.getroot()

    categories = []
    for category in root.find('categories').getchildren():
        categories.append(load_category(category))

    accessories = []
    for accessory in root.find('accessories').getchildren():
        accessories.append(load_accessory(accessory))

    weapons = []
    for weapon in root.find('weapons').getchildren():
        weapons.append(load_weapon(weapon))